﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bankomat_bb
{
    public partial class KartenEntnahme : Form
    {
        //Zustand 5: Bankomat wartet auf die Entnahme der Karte
        public KartenEntnahme()
        {
            InitializeComponent();
        }

        private void GeheInZustandKartenEntnahme()
        {
            var stringBuilder = new StringBuilder();

            if (BaseMenu.PwFalsch >= 3) stringBuilder.AppendLine("Sie haben 3 Mal den falschen PIN eingegeben");
            if (!BaseMenu.KarteValid) stringBuilder.AppendLine("Ihre Karte ist nicht valid.");
            stringBuilder.AppendLine("Vielen Dank für Ihren Besuch.");

            lblAnleitung.Text = stringBuilder.ToString();
        }

        private void KartenEntnahme_Load(object sender, EventArgs e)
        {
            Location = new Point(200, 200);
            GeheInZustandKartenEntnahme();
        }

        private void KarteAus_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}