﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bankomat_bb
{
    //Zustand 1: Bankomat wartet auf die Eingabe der Karte
    public partial class BaseMenu : Form
    {
        public static PinEingabe FrmPinEingabe = new PinEingabe();
        public static Menu FrmMenu = new Menu();
        public static UnterMenuSaldo FrmUnterMenuSaldo = new UnterMenuSaldo();
        public static KartenEntnahme FrmKartenEntnahme = new KartenEntnahme();

        public static bool KarteValid = true;
        public static double Saldo = 0;
        public static int PwFalsch;

        public static string DefaultPassword = "1234";

        public BaseMenu()
        {
            InitializeComponent();
            FrmPinEingabe.SetzePasswort(DefaultPassword);
        }

        private void EingebenKartendaten(object sender, EventArgs e)
        {
            Location = new Point(200, 200);
            KarteValid = chkKarteValid.Checked;
            FrmPinEingabe.SetzePasswort(txtPWStart.TextLength > 0 ? txtPWStart.Text : DefaultPassword);
            Saldo = (txtSaldo.TextLength) > 0 ? Convert.ToDouble(txtSaldo.Text) : 0;
        }

        private void KarteEin_Click(object sender, EventArgs e)
        {
            if (KarteValid)
            {
                FrmPinEingabe.Show();
            }
            else
            {
                FrmKartenEntnahme.Show();
            }
        }
    }
}