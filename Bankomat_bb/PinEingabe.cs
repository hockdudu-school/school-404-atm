﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bankomat_bb
{
    public partial class PinEingabe : Form
    {
        //Zustand 2: Bankomat wartet auf die Eingabe der PIN

        string _passwort;

        public PinEingabe()
        {
            InitializeComponent();
        }

        private void GeheInZustandPinErwarten()
        {
            txtPW.Text = "";
            lblAnleitung.Text = "Bitte geben Sie Ihr Passwort ein";
            BaseMenu.PwFalsch = 0;
        }

        public void SetzePasswort(string pw)
        {
            _passwort = pw;
        }

        private void PinEingabe_Load(object sender, EventArgs e)
        {
            Location = new Point(200, 200);
            GeheInZustandPinErwarten();
        }

        private void ButtonPW_Click(object sender, EventArgs e)
        {
            if (txtPW.Text == _passwort)
            {
                BaseMenu.FrmMenu.Show();
                Hide();
            }
            else
            {
                BaseMenu.PwFalsch += 1;
                if (BaseMenu.PwFalsch >= 3)
                {
                    BaseMenu.FrmKartenEntnahme.Show();
                    Hide();
                }
                else
                {
                    txtPW.Text = "";
                    lblAnleitung.Text =
                        $"Der Pin ist falsch, versuchen Sie es erneut. Sie haben noch {Convert.ToString(3 - BaseMenu.PwFalsch)} Versuch(e)";
                }
            }
        }

        private void btnAbbruch_Click(object sender, EventArgs e)
        {
            BaseMenu.FrmKartenEntnahme.Show();
            Hide();
        }
    }
}